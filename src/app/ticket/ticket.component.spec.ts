import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TicketComponent } from './ticket.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


xdescribe('TicketComponent', () => {
  let component: TicketComponent;
  let fixture: ComponentFixture<TicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TicketComponent],
      imports: [
        ReactiveFormsModule,
        NgbModule,
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form reporte ticket should be valid', async(() => {
    component.form_reporter.controls['name'].setValue("Prueba");
    component.form_reporter.controls['lastname'].setValue("Unitaria");
    component.form_reporter.controls['email'].setValue("prueba@gmail.com");
    expect(component.form_reporter.valid).toBeTruthy();
  }))

  it('form reporte ticket debe contener name, lastname y email', () => {
    expect(component.form_reporter.contains('name')).toBeTruthy();
    expect(component.form_reporter.contains('lastname')).toBeTruthy();
    expect(component.form_reporter.contains('email')).toBeTruthy();
  });

  it('form reporte ticket campos deben ser obligatorias', () => {
    component.form_reporter.controls['name'].setValue("");
    component.form_reporter.controls['lastname'].setValue("");
    component.form_reporter.controls['email'].setValue("");
    expect(component.form_reporter.valid).toBeFalsy();
  });

  it('form reporte ticket email debe ser valido', () => {
    component.form_reporter.controls['name'].setValue("Prueba");
    component.form_reporter.controls['lastname'].setValue("Unitaria");
    component.form_reporter.controls['email'].setValue("prueba@");
    expect(component.form_reporter.valid).toBeFalsy();
  });

  it('form ticket campos validos', () => {
    expect(component.form_ticket.contains('summary')).toBeTruthy();
    expect(component.form_ticket.contains('description')).toBeTruthy();
    expect(component.form_ticket.contains('priority')).toBeTruthy();
    expect(component.form_ticket.contains('state')).toBeTruthy();
    expect(component.form_ticket.contains('reporter_id')).toBeTruthy();
    expect(component.form_ticket.contains('asignee_id')).toBeTruthy();
    expect(component.form_ticket.contains('department_id')).toBeTruthy();
  });

  it('form ticket should be valid', async(() => {
    component.form_ticket.controls['summary'].setValue("Resumen Prueba");
    component.form_ticket.controls['description'].setValue("Descipcion Unitaria");
    component.form_ticket.controls['priority'].setValue("Alta");
    component.form_ticket.controls['state'].setValue("NEW");
    component.form_ticket.controls['reporter_id'].setValue("1");
    component.form_ticket.controls['asignee_id'].setValue("1");
    component.form_ticket.controls['department_id'].setValue("1");
    expect(component.form_ticket.valid).toBeTruthy();
  }))


  it('form ticket campos no deben estar vacios', async(() => {
    component.form_ticket.controls['summary'].setValue("");
    component.form_ticket.controls['description'].setValue("");
    component.form_ticket.controls['priority'].setValue("");
    component.form_ticket.controls['state'].setValue("");
    component.form_ticket.controls['reporter_id'].setValue("");
    component.form_ticket.controls['asignee_id'].setValue("");
    component.form_ticket.controls['department_id'].setValue("");
    expect(component.form_ticket.valid).toBeFalsy();
  }))

});


