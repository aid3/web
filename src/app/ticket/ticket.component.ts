import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { ReporterService } from '../services/reporter.service';
import { DepartamentoService } from '../services/departamento.service';
import { Departamento } from '../models/departamento';
import { TicketService } from '../services/ticket.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  
  // Forms
  form_reporter: FormGroup;
  form_ticket: FormGroup;

  // Lista de departamentso
  depto_list: Departamento[];

  //## Toast mensajes reporter
  successMessage:string = '';
  errorMessage:string = '';
  warningMessage:string = '';

  //## toast mensajes Ticket
  successMessageTicket:string = '';
  errorMessageTicket:string = '';
  warningMessageTicket:string = '';

  //
  reporter_id:number = 0;
  current_token:number = 0;

  constructor(public formBuilder:FormBuilder, 
              private ticketService:TicketService, 
              private reporterService:ReporterService,
              private deptoService:DepartamentoService,
              private modalService: NgbModal) {
    this.set_form_reporter();
  }

  ngOnInit() { 
    this.lista_depto();
  }

  generar_token(content): void {
    // Para generar token, se crea un reporter y este devuelve un token

    if(!this.validar_form_reporter()) {
      return;
    }
    
    this.crear_reporter(content);
  }

  crear_reporter(content) {
    this.reporterService.create(this.form_reporter.value).subscribe(data => {
      if (data['user']) {
        this.current_token = data['token'];
        let user = data['user'];
        this.reporter_id = user.id;

        // Si el reporter se creó, abre el formulario para el ticket
        this.set_form_ticket();
        this.open(content);
      } else {
        this.errorMessage = 'Los datos han sido rechazados';
      }
    });
  }

  enviar_ticket() {
    // Para crear el ticket, valída que el tóken sea valido
    this.reporterService.getToken(this.current_token).subscribe(res => {
      let flag:boolean = res['valid'];

      if(flag) {
        if(this.validar_form_ticket()) {
          this.successMessageTicket = 'Token válido';
          this.nuevo_ticket();
        }

      } else {
        this.errorMessageTicket = 'El Token ya se ha utilizado';
      }
      
    });
  }

  nuevo_ticket() {
    this.ticketService.create(this.form_ticket.value).subscribe(data =>{
      if (data.id) {
        this.successMessageTicket = 'Se ha generado el ticket y se ha enviado el reporte';
        this.set_form_ticket();
      } else {
        this.errorMessageTicket = 'Ha ocurrido un error al enviar el formulario'
      }
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  lista_depto(): void {
    this.deptoService.get().subscribe(data => {
      this.depto_list = data;
    });
  }

  validar_form_reporter():boolean {
    if (this.form_reporter.valid) {
      return true;
    }

    // Email
    if (this.form_reporter.get('email').hasError('email')) {
      this.errorMessage = 'El formato del email es inválido';
    } else {
      this.errorMessage = 'Debe llenar todos los campos';
    }

    return false;
  }

  validar_form_ticket(): boolean {
    if (this.form_ticket.valid) {
      return true;
    }

    this.errorMessageTicket = 'Debe llenar todos los campos';
    return false;
  }

  set_form_reporter(): void {
    this.form_reporter = this.formBuilder.group({
      name: ['', [Validators.required]],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  set_form_ticket(): void {
    this.form_ticket = this.formBuilder.group({
      summary: ['', [Validators.required]],
      description: ['', [Validators.required]],
      priority: ['', [Validators.required]],
      state: ['NEW', [Validators.required]],
      reporter_id: [this.reporter_id, [Validators.required]],
      asignee_id: ['1', [Validators.required]],
      department_id: ['', [Validators.required]]
    });
  }

}
