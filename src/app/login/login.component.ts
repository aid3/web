import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  successMessage:string = '';
  errorMessage:string = '';

  constructor(public formBuilder: FormBuilder,
              public authService: AuthService,
              public router: Router) { 
    this.set_form();
  }

  ngOnInit() {
  }

  set_form(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  login(): void {
    if (!this.validar_form()) {
      return;
    }

    this.authService.login(this.form.value).subscribe(data => {
      if (data['status']) {
        this.successMessage = '¡Acceso correcto!'
        this.router.navigate(['/home']);
      }
      else if (data['message']) {
        this.errorMessage = data['message'];
      }
      else {
        this.errorMessage = 'Ha ocurrido un error inesperado! :(';
      }
    });
  }

  validar_form(): boolean {
    if (this.form.valid) { return true; }

    // Ambos
    if (this.form.get('email').hasError('required') && this.form.get('password').hasError('required')) {
      this.errorMessage = 'No se han ingresado datos';
      return false;    
    }

    // Email
    if (this.form.get('email').hasError('required')) {
      this.errorMessage = 'El campo email es obligatorio';
      return false;  
    } 
    else if (this.form.get('email').hasError('email')) {
      this.errorMessage = 'El formato del email es inválido';
      return false;  
    }

    // Password
    if (this.form.get('password').hasError('required')) {
      this.errorMessage = 'El campo contraseña es obligatorio';   
      return false;  
    }
  }
}
