import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap' ; 
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ExpectedConditions } from 'protractor';
import { AuthService } from '../services/auth.service';
import { NavigationEnd, Router } from '@angular/router';

class FakeRouter {
  navigate(params){

  }
}
class FakeService {
login(params){

}
}
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        ReactiveFormsModule,
        NgbModule,
        HttpClientModule
      ],
      providers:[
        {provide:Router, useClass: FakeRouter},
        {provide: AuthService, useClass:FakeService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.set_form();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('Debe contener email y el campo de password',() => {
    expect(component.form.contains('email')).toBeTruthy();
    expect(component.form.contains('password')).toBeTruthy();
  });

  it('email debe ser obligatorio',() => {
     const control =  component.form.get('email');
     control.setValue('');
     expect(control.valid).toBeFalsy();
  });
  
  it('email debe ser un email valido',() => {
    const control =  component.form.get('email');
    control.setValue('patogato@');
    expect(control.valid).toBeFalsy();
 });

  it('password debe ser obligatorio',() => {
    const control =  component.form.get('password');
    control.setValue('');
    expect(control.valid).toBeFalsy();
  });


});
