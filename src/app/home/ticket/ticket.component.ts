import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/models/ticket';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  ticket_list: Ticket[];

  constructor(private ticketService: TicketService) { }

  ngOnInit() {
    this.lista_ticket()
  }

  lista_ticket(): void {
    this.ticketService.get().subscribe(data => {
      this.ticket_list = data;
    });
  }

}
