import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Departamento } from 'src/app/models/departamento';
import { Empleado } from 'src/app/models/empleado';
import { DepartamentoService } from 'src/app/services/departamento.service';
import { EmpleadoService } from 'src/app/services/empleado.service';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {
  form: FormGroup;
  empleado_list: Empleado[];
  depto_list: Departamento[];

  form_titulo: string;
  modo_crear: boolean;

  //##
  successMessage:string = '';
  errorMessage:string = '';
  warningMessage:string = '';
  
  constructor(private empleadoService: EmpleadoService,
              private deptoService: DepartamentoService,
              private formBuilder: FormBuilder) { 
    this.form_titulo = 'Agregar nuevo empleado';
    this.modo_crear = true;
    this.set_form(new Empleado());
  }

  ngOnInit() {
    this.lista_depto();
    this.lista_empleado();
  }

  set_form(empleado: Empleado):void {
    this.form = this.formBuilder.group({
      id: [empleado.id],
      name: [empleado.name, [Validators.required]],
      lastname: [empleado.lastname, [Validators.required]],
      email: [empleado.email, [Validators.required, Validators.email]],
      password: [empleado.password, [Validators.required]],
      department_id: [empleado.department_id, [Validators.required]],
    });
  }

  guardar_empleado(): void {
    if (!this.validar_form()) {
      return;
    }

    if (this.modo_crear) {
      this.crear_empleado();
    }
    else {
      this.editar_empleado();
    }
  }

  nuevo_dato(): void {
    this.modo_crear = true;
    this.form_titulo = 'Agregar nuevo empleado';
    this.set_form(new Empleado());
  }

  seleccion_empleado(empleado: Empleado): void {
    this.modo_crear = false;
    this.form_titulo = 'Editar empleado';
    this.set_form(empleado);
  }

  lista_empleado(): void {
    this.empleadoService.get().subscribe(data => {
      this.empleado_list = data;
    });
  }

  lista_depto(): void {
    this.deptoService.get().subscribe(data => {
      this.depto_list = data;
    });
  }

  crear_empleado(): void {
    this.empleadoService.create(this.form.value).subscribe(data => {
      if(data['status']) {
        this.successMessage = '¡Usuario Creado!'
        this.set_form(new Empleado());
        this.lista_empleado();
      } else if(data['message']) {
        this.errorMessage = data['message']
      } else {
        this.errorMessage = 'Ha ocurrido un error inesperado! :(';
      }      
    });
  }

  editar_empleado(): void {
    this.empleadoService.edit(this.form.value).subscribe(data => {
      if(data['id']) {
        this.successMessage = '¡Datos actualizados!'
        this.lista_empleado();
      } else {
        this.errorMessage = 'No se han actualizado los datos :(';
      }
    });
  }

  eliminar_empleado(empleado: Empleado): void { }

  validar_form(): boolean {
    if (this.form.valid) { return true; }

    if (this.form.get('email').hasError('email')) {
      this.errorMessage = 'El formato del email es inválido';
      return false;  
    } else {
      this.errorMessage = 'Debe llenar todos los campos'
    }

  }

}
