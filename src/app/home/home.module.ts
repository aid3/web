import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { SidebarComponent } from '../shared/sidebar/sidebar.component';
import { FooterComponent } from '../shared/footer/footer.component';
import { SpinnerComponent } from '../shared/spinner/spinner.component';
import { PerfilComponent } from './perfil/perfil.component';
import { DepartamentoComponent } from './departamento/departamento.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmpleadoComponent } from './empleado/empleado.component';
import { TicketComponent } from './ticket/ticket.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    HomeComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    SpinnerComponent,
    PerfilComponent,
    DepartamentoComponent,
    EmpleadoComponent,
    TicketComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class HomeModule { }
