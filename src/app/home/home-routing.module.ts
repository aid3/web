import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketComponent } from './ticket/ticket.component';
import { DepartamentoComponent } from './departamento/departamento.component';
import { EmpleadoComponent } from './empleado/empleado.component';

import { HomeComponent } from './home.component';
import { PerfilComponent } from './perfil/perfil.component';

const routes: Routes = [
  { path: '', component: HomeComponent, 
    children: [ 
      { path: 'perfil', component: PerfilComponent },
      { path: 'departamentos', component: DepartamentoComponent },
      { path: 'empleados', component: EmpleadoComponent },
      { path: 'tickets', component: TicketComponent },
      { path: '', redirectTo: '/home/perfil', pathMatch: 'full' }
    ]},
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
