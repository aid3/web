import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Departamento } from 'src/app/models/departamento';
import { DepartamentoService } from 'src/app/services/departamento.service';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.scss']
})
export class DepartamentoComponent implements OnInit {
  form: FormGroup;
  depto_list : Departamento [];

  form_titulo: string;
  modo_crear: boolean;
  
  constructor(private deptoService: DepartamentoService,
              private formBuilder: FormBuilder) {
    this.form_titulo = 'Agregar nuevo departamento';
    this.modo_crear = true;
    this.set_form(new Departamento());
  }

  ngOnInit() {
    this.lista_depto()
  }

  set_form(depto: Departamento):void {
    this.form = this.formBuilder.group({
      id: [depto.id],
      name: [depto.name, [Validators.required]],
      description: [depto.description, [Validators.required]],
    });
  }

  guardar_depto(): void {
    if (!this.form.valid) {
      console.error('faltan campos');
      return;
    }

    if (this.modo_crear) {
      this.crear_depto();
    }
    else {
      this.editar_depto();
    }
  }

  nuevo_dato(): void {
    this.modo_crear = true;
    this.form_titulo = 'Agregar nuevo departamento';
    this.set_form(new Departamento());
  }

  seleccion_depto(depto: Departamento): void {
    this.modo_crear = false;
    this.form_titulo = 'Editar departamento';
    this.set_form(depto);
  }

  lista_depto(): void {
    this.deptoService.get().subscribe(data => {
      this.depto_list = data;
    });
  }

  crear_depto(): void {
    this.deptoService.create(this.form.value).subscribe(data => {
      this.set_form(new Departamento());
      this.lista_depto();
    });
  }

  editar_depto(): void {
    this.deptoService.edit(this.form.value).subscribe(data => {
      this.lista_depto();
    });
  }

  eliminar_depto(depto: Departamento): void { }

}
