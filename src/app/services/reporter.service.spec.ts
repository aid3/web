import { TestBed } from '@angular/core/testing';

import { ReporterService } from './reporter.service';

xdescribe('ReporterService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ReporterService]
  }));

  it('should be created', () => {
    const service: ReporterService = TestBed.get(ReporterService);
    expect(service).toBeTruthy();
  });
});
