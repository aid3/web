import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empleado } from '../models/empleado';
import { ApiService } from './api.service';
import { IService } from './IService';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService implements IService {

  endpoint:string = 'users';
  constructor(private api: ApiService) { }

  get(): Observable<any> {
    return this.api.get(this.endpoint);
  }

  getId(id: number): Observable<any> {
    return this.api.get(this.endpoint + '/' + id);
  }

  create(empleado: Empleado): Observable<any> {
    return this.api.post(this.endpoint, empleado)
  }

  edit(empleado: Empleado): Observable<any> {
    return this.api.put(this.endpoint, empleado)
  }

  delete(id: number): Observable<any> {
    return this.api.delete(this.endpoint)
  }

}
