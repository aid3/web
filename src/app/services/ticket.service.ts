import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ticket } from '../models/ticket';
import { ApiService } from './api.service';
import { IService } from './IService';

@Injectable({
  providedIn: 'root'
})
export class TicketService implements IService {

  endpoint:string = 'tickets'
  constructor(private api: ApiService) { }

  get(): Observable<any> {
    return this.api.get(this.endpoint);
  }

  getId(id: number): Observable<any> {
    return this.api.get(this.endpoint + '/' + id);
  }

  create(ticket: Ticket): Observable<any> {
    return this.api.post(this.endpoint, ticket)
  }

  edit(ticket: Ticket): Observable<any> {
    return this.api.put(this.endpoint, ticket)
  }

  delete(id: number): Observable<any> {
    return this.api.delete(this.endpoint)
  }

}
