import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reporter } from '../models/reporter';
import { ApiService } from './api.service';
import { IService } from './IService';

@Injectable({
  providedIn: 'root'
})
export class ReporterService implements IService {

  endpoint:string = 'reporters';
  constructor(private api: ApiService) { }

  getToken(token:any): Observable<any> {
    return this.api.post('tokens', { "token": token});
  }

  get(): Observable<any> {
    return this.api.get(this.endpoint);
  }

  getId(id: number): Observable<any> {
    return this.api.get(this.endpoint + '/' + id);
  }

  create(reporter: Reporter): Observable<any> {
    return this.api.post(this.endpoint, reporter)
  }

  edit(reporter: Reporter): Observable<any> {
    return this.api.put(this.endpoint, reporter)
  }

  delete(id: number): Observable<any> {
    return this.api.delete(this.endpoint)
  }

}
