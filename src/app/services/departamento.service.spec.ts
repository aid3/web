import { TestBed } from '@angular/core/testing';

import { DepartamentoService } from './departamento.service';

xdescribe('DepartamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [DepartamentoService]
  }));

  it('should be created', () => {
    const service: DepartamentoService = TestBed.get(DepartamentoService);
    expect(service).toBeTruthy();
  });
});
