import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Departamento } from '../models/departamento';
import { ApiService } from './api.service';
import { IService } from './IService';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService implements IService {
  
  endpoint:string = 'departments';
  constructor(private api: ApiService) { }

  get(): Observable<any> {
    return this.api.get(this.endpoint);
  }

  getId(id: number): Observable<any> {
    return this.api.get(this.endpoint + '/' + id);
  }

  create(depto: Departamento): Observable<any> {
    return this.api.post(this.endpoint, depto)
  }

  edit(depto: Departamento): Observable<any> {
    return this.api.put(this.endpoint, depto)
  }

  delete(id: number): Observable<any> {
    return this.api.delete(this.endpoint)
  }

}
