import { TestBed } from '@angular/core/testing';

import { EmpleadoService } from './empleado.service';

xdescribe('EmpleadoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [EmpleadoService]
  }));

  it('should be created', () => {
    const service: EmpleadoService = TestBed.get(EmpleadoService);
    expect(service).toBeTruthy();
  });
});
