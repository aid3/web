import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { TicketService } from './ticket.service';

xdescribe('TicketService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [TicketService]
  }));

  it('should be created', () => {
    const service: TicketService = TestBed.get(TicketService);
    expect(service).toBeTruthy();
  });
});
