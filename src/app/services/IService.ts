import { Observable } from 'rxjs';

export interface IService {

    get(): Observable<any>;
    getId(id: number): Observable<any>;
    create(item: any): Observable<any>;
    edit(item: any): Observable<any>;
    delete(id: number): Observable<any>;

}