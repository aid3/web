export class Reporter {
    name:string;
    lastname:string;
    email:string;
    access_token:number;
    id:number;
}
