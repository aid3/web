export class Ticket {
    id: number;
    summary: string;
    description: string;
    priority: string;
    state: string;
    reporter_id: number;
    asignee_id: number;
    department_id: number;
}
