export class Empleado {
    id: number;
    name: string;
    lastname: string;
    email: string;
    password: string;
    department_id: number;
}
