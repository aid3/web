export class Departamento {
    id: number;
    name: string;
    description: string;
}
